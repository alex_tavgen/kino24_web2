import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SubpageService } from './subpage.service';
import { SubpageModel } from '../model/subpage.model';

@Component({
  selector: 'subpage',
  templateUrl: 'subpage.component.html'
})
export class SubpageComponent implements OnInit {
  public name: string;
  public page: SubpageModel;

  constructor(private service: SubpageService, public route: ActivatedRoute, private router: Router) {
    this.service.getSubpages();
  }

  public ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      if (params['name'] !== undefined) {
        let name = params['name'];
        this.service.setSubpage(name);
      }
    });

  }

}
