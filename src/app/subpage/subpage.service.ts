import { Injectable } from '@angular/core';
import { ApiService } from '../services/api.service';
import { SubpageModel } from '../model/subpage.model';

@Injectable()
export class SubpageService {
  public pages: SubpageModel[];
  public page: SubpageModel;

  constructor(private api: ApiService) {
  }

  public getSubpages() {
    return this.api.getSubpages()
      .subscribe(
        (data) => {
          this.pages = data;
        }
      );
  }

  public setSubpage(name: string) {
    this.page = this.pages.filter((x) => x.name === name)[0];
  }

}
