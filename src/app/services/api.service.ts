import { Injectable } from '@angular/core';
import { isDevMode } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {

  constructor(private http: Http) {
  }

  public getMovies()  {
    return this.http.get(this.getBackendUrlBase() + '/list')
      .map((res) => res.json());
  }

  public getMoviesDate(date)  {
    return this.http.get(this.getBackendUrlBase() + '/list/' + date +'/')
      .map((res) => res.json());
  }

  public getCities()  {
    return this.http.get(this.getBackendUrlBase() + '/api/v1/cities/')
      .map((res) => res.json());
  }

   public getDates()  {
    return this.http.get(this.getBackendUrlBase() + '/dates/')
      .map((res) => res.json());
  }

  public getCinemas()  {
    return this.http.get(this.getBackendUrlBase() + '/api/v1/cinemas/')
      .map((res) => res.json());
  }

  public getSubpages()  {
    return this.http.get(this.getBackendUrlBase() + '/api/v1/subpages/')
      .map((res) => res.json());
  }

  public getHeroSlides()  {
    return this.http.get(this.getBackendUrlBase() + '/api/v1/hero_slides/')
      .map((res) => res.json());
  }

  private getBackendUrlBase() {
    if (isDevMode()) {
      return 'http://localhost:8000';
    } else {
      return 'https://kino24-210708.appspot.com';
    }
  }
}
