import { Injectable } from '@angular/core';
import { CookieStorage } from 'cookie-storage';

@Injectable()
export class SessionStorageService {
  public static DefaultValue(arg1: any, arg2: any) {
    return (typeof arg1 === 'undefined') ? arg2 : arg1;
  }

  public sessionStorage: any;

  constructor() {
      this.sessionStorage = window.sessionStorage;
  }

  public set(key: string, value: string): void {
    this.sessionStorage.setItem(key, value);
  }

  public get(key: string, defaultValue: string): string {
    return this.sessionStorage.getItem(key) || defaultValue;
  }

  public setObject(key: string, value: any): void {
    this.set(key, JSON.stringify(value));
  }

  public getObject(key: string): any {
    let val = this.get(key, null);
    if (!val) {
      return null;
    }
    let obj = JSON.parse(val);
    if (obj) {
      return obj;
    } else {
      return null;
    }
  }

  public remove(key: string): any {
    this.sessionStorage.removeItem(key);
  }
}
