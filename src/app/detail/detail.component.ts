import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router }   from '@angular/router';
import { DetailService } from './detail.service';
import { SafeResourceUrl } from './saferesource.pipe';

@Component({
  selector: 'detail',
  templateUrl: 'detail.component.html'
})
export class DetailComponent implements OnInit {
  constructor(public detail: DetailService, public route: ActivatedRoute, private router: Router) {
    this.detail.getCinemas();
  }

  public ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      if (params['id'] !== undefined) {
        let id = +params['id'];
        this.detail.getMovie(id);
      }
    });
  }

  public backToMainPage() {
    this.router.navigate(['/']);
  }

  public getNameCinema(id: number) {
    let cinema = this.detail.cinemas.filter((x) => x.id === id)[0];
    return cinema ? cinema.name : 'Unknown name';
  }
}
