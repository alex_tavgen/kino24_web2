import { Injectable } from '@angular/core';
import { ApiService } from '../services/api.service';
import { MovieModel } from '../model/movie.model';
import { CinemaModel }from '../model/cinema.model';

@Injectable()
export class DetailService {
  public movie: MovieModel;
  public cinemas: CinemaModel[];

  constructor(private api: ApiService) {
  }

  public getMovie(id: number) {
    return this.api.getMovies()
      .subscribe((data) => {
        this.movie = data.filter((x) => x.id === id)[0];
        if (this.movie) {
          this.movie.durationStr = this.getDurationStr(this.movie.duration);
          this.getSchedule();
        }
      });
  }

  public getCinemas() {
    return this.api.getCinemas()
      .subscribe(
        (data) => {
          this.cinemas = data;
        }
      );
  }

  private getDurationStr(duration: number) {
    let hours = Math.floor(duration / 60);
    let minutes = duration % 60;
    return `${hours}h ${minutes}min`;
  }

  private getSchedule() {
    this.movie.showtimes.forEach((cinema) => {
      let timetable = [];

      for (let prop in cinema.schedule) {
        if (cinema.schedule.hasOwnProperty(prop)) {
          cinema.schedule[prop].forEach((time) => {
            if (!this.movie.has3D) {
              this.movie.has3D = this.has3D(time);
            }
            timetable.push(this.removeUnnecessary(time));
          });
        }
      }

      cinema.scheduleArray = this.distinct(timetable);
      cinema.scheduleArray.sort();
    });
  }

  private has3D(str: string) {
    return str.indexOf('3D') !== -1;
  }

  private removeUnnecessary(str: string) {
    let pattern = /[0-2]\d:[0-5]\d/;
    let result = str.match(pattern);
    if (result) {
      return result[0];
    }

    return null;
  }

  private distinct(array) {
    let n = {};
    let result = [];
    array.forEach((item) => {
      if (!n[item]) {
        n[item] = true;
        result.push(item);
      }
    });

    return result;
  }
}
