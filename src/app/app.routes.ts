import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

import { DataResolver } from './app.resolver';
import { DetailComponent } from "./detail/detail.component";

import { SubpageComponent } from "./subpage/subpage.component";

export const ROUTES: Routes = [
  { path: '',      component: HomeComponent },
  { path: 'home',  component: HomeComponent },

  { path: 'page/:name',  component: SubpageComponent },

  { path: 'movie/:id',  component: DetailComponent },
  { path: '**', redirectTo: '', pathMatch: 'full'},
];
