import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { HeroModel } from '../model/hero.model';

@Component({
  selector: 'hero',
  templateUrl: 'hero.component.html'
})
export class HeroComponent implements OnInit {
  public slides: HeroModel[];

  constructor(public api: ApiService) {
  }

  public ngOnInit(): void {

  }

  public getCinemas() {
    return this.api.getHeroSlides()
      .subscribe(
        (data) => {
          this.slides = data;
        }
      );
  }

}
