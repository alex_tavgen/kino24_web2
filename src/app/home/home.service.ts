import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { ApiService } from '../services/api.service';
import { CityModel } from '../model/city.model';
import { CinemaModel } from '../model/cinema.model';
import { RestrictionModel } from '../model/restriction.model';
import { MovieModel } from '../model/movie.model';
import {forEach} from "@angular/router/src/utils/collection";
import {SessionStorageService} from "../services/session-storage.service";

@Injectable()
export class HomeService {
  public cities: CityModel[];
  public cinemas: CinemaModel[];
  public restrictions: RestrictionModel[];
  public dates: Date[];
  public movies: MovieModel[];
  public filteredMovies: any;
  public searchQuery: any;
  public activeCinemaId: number;

  constructor(private api: ApiService, private storage: SessionStorageService, private http: Http) {
    this.getFromStorage();
  }

  public getCities() {
    return this.api.getCities()
      .subscribe(
        (data) => {
          this.cities = data;
        }
      );
  }
  public getDates() {
    return this.api.getDates()
      .subscribe(
        (data) => {
          this.dates = data;
        }
      );
  }
  public getCinemas() {
    return this.api.getCinemas()
      .subscribe(
        (data) => {
          this.cinemas = data;
        }
      );
  }
  public getRestrictions() {
    this.restrictions = [
      {order: 1, name: 'Family'},
      {order: 1, name: '0+'},
      {order: 3, name: '6+'},
      {order: 4, name: 'K-6'},
      {order: 5, name: '12+'},
      {order: 6, name: 'K-12'},
      {order: 6, name: 'MS-12'},
      {order: 7, name: '14+'},
      {order: 8, name: 'K-14'},
      {order: 9, name: '16+'},
      {order: 10, name: 'K-16'},
      {order: 11, name: '18+'},
      {order: 12, name: 'K-18'}
    ];
  }

  public getMovies() {
    let moviesData;
    if (this.searchQuery && this.searchQuery.date) {
      moviesData = this.api.getMoviesDate(this.searchQuery.date);
    } else {
      moviesData = this.api.getMovies();
    }
    return moviesData
      .subscribe(
        (data) => {
          let movies: MovieModel[];
          data.forEach((movie) => {
            this.getSchedule(movie);
          });
          this.movies = data;
          this.filterMovies();
        }
      );
  }

  public keyDownFunction(event) {
    if(event.keyCode == 13) {
      this.getMovies();
    }
  }

  public clearSearchParameters() {
    this.setDefault();
    this.getMovies();
  }

  public filterMovies() {
    this.putToStorage(); // saving searchQuery

    this.filteredMovies = this.cloneArray(this.movies);

    if (!this.searchQuery) {
      this.embedBanners();
      return;
    }

    if (this.searchQuery.title && this.notEmptyArray(this.filteredMovies)) {
      this.filteredMovies = this.filteredMovies
        .filter((movie) =>
          movie.title.toLowerCase().indexOf(this.searchQuery.title.toLowerCase()) !== -1
          || movie.title_eng.toLowerCase().indexOf(this.searchQuery.title.toLowerCase()) !== -1
        );
    }

    if (this.searchQuery.restriction && this.notEmptyArray(this.filteredMovies)) {
      let searchedRestriction = this.restrictions.find(r => r.name === this.searchQuery.restriction);
      this.filteredMovies = this.filteredMovies
        .filter((movie) => {
          let movieRestriction = this.restrictions.find(r => r.name === movie.restriction);
          if (movieRestriction) {
            return movieRestriction.order >= searchedRestriction.order;
          }
        });
    }

    let startSessionRangeStart = this.searchQuery.rangeSessionStart[0];
    let startSessionRangeEnd = this.searchQuery.rangeSessionStart[1];

    if (this.notEmptyArray(this.filteredMovies)) {
      this.filteredMovies = this.filteredMovies
        .filter((movie) => {
          movie.showtimes.forEach((cinema, index) => {
            cinema.scheduleArray = cinema.scheduleArray.filter((time) => {
              let h = +time.split(':')[0];
              return startSessionRangeStart <= h && h <= startSessionRangeEnd;
            });
          });
          movie.showtimes = movie.showtimes
            .filter((cinema) => {
              return this.notEmptyArray(cinema.scheduleArray);
            })
          return this.notEmptyArray(movie.showtimes);
        });
    }

    if (this.searchQuery.cinemaId && this.notEmptyArray(this.filteredMovies)) {
      var id = +this.searchQuery.cinemaId;
      this.activeCinemaId = id;
      this.filteredMovies = this.filteredMovies
        .filter((movie) => {
          let hasCinema = false;
          movie.showtimes.forEach((x) => {
            if (x.cinema === id) {
              hasCinema = true;
            }
          });
          return hasCinema;
        });
    } else {
      this.activeCinemaId = null;
    }

    this.embedBanners();
  }

  private notEmptyArray(array) {
    return (array && array.length > 0);
  }

  private embedBanners() {
    if (this.filteredMovies && this.filteredMovies.length > 6) {
      let banner = {
        type: 'adv',
        poster: 'assets/img/banner1.png',
        size: 2
      };
      this.filteredMovies.splice(4, 0, banner);
    }

    if (this.filteredMovies && this.filteredMovies.length > 10) {
      let banner = {
        type: 'adv',
        poster: 'assets/img/banner2.png',
        size: 4
      };
      this.filteredMovies.splice(11, 0, banner);
    }
  }

  private cloneArray(currentArray) {
    let newArray = [];
    currentArray.forEach((item) => {
      newArray.push({...item});
    });
    return newArray;
  }

  private getFromStorage(): void {
    try {
      let restoredSearchQuery = this.storage.getObject('searchQuery');
      if (restoredSearchQuery) {
        this.searchQuery = restoredSearchQuery;
        return;
      }
    } catch (e) {
      if (console) {
        console.log(e);
      }
    }
    this.setDefault();
  }

  private putToStorage(): void {
    this.storage.setObject('searchQuery', this.searchQuery);
  }

  private setDefault(): void {
    this.searchQuery = {
      title: '',
      rangeSessionStart: [10, 20],
      rangeIMDB: [5, 10]
    };
  }

  private getSchedule(movie: MovieModel) {
    movie.showtimes.forEach((cinema) => {
      let timetable = [];

      for (let prop in cinema.schedule) {
        if (cinema.schedule.hasOwnProperty(prop)) {
          cinema.schedule[prop].forEach((time) => {
            if (!movie.has3D) {
              movie.has3D = this.has3D(time);
            }
            timetable.push(this.removeUnnecessary(time));
          });
        }
      }

      cinema.scheduleArray = this.distinct(timetable);
      cinema.scheduleArray.sort();
    });

    return movie;
  }

  private has3D(str: string) {
    return str.indexOf('3D') !== -1;
  }

  private removeUnnecessary(str: string) {
    let pattern = /[0-2]\d:[0-5]\d/;
    let result = str.match(pattern);
    if (result) {
      return result[0];
    }

    return null;
  }

  private distinct(array) {
    let n = {};
    let result = [];
    array.forEach((item) => {
      if (!n[item]) {
        n[item] = true;
        result.push(item);
      }
    });

    return result;
  }
}
