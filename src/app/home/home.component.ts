import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';

@Component({
  selector: 'home',
  templateUrl: 'home.component.html'
})
export class HomeComponent implements OnInit {

  public showAdditionalParameters: boolean = false;
  public activeCinemaId;

  constructor(public home: HomeService) {
    this.home.getCities();
    this.home.getCinemas();
    this.home.getMovies();
    this.home.getDates();
    this.home.getRestrictions();
  }

  public ngOnInit(): void {
    this.home.clearSearchParameters();
  }

  public isActiveCinema(id: number, index: number) {
    if (!this.home.activeCinemaId) {
      return index == 0 ? true : false;
    }
    return id == this.home.activeCinemaId;
  }

  public getNameCinema(id: number) {
    let cinema = this.home.cinemas.filter((x) => x.id === id)[0];
    return cinema ? cinema.name : '';
  }

  public formatDate(dateString: string) {
    let options = { weekday: 'narrow', year: 'numeric', month: 'numeric', day: 'numeric' };
    let optionsShort = { year: 'numeric', month: 'numeric', day: 'numeric' };

    let date = new Date(dateString);

    let today = new Date();
    today.setHours(0,0,0,0)
    let tomorrow = new Date();
    tomorrow.setDate(today.getDate() + 1);
    tomorrow.setHours(0,0,0,0)

    if (date.toDateString() == today.toDateString()) {
      return "Täna, " + date.toLocaleDateString('et-EE', optionsShort);
    }
    else if (date.toDateString() == tomorrow.toDateString()) {
      return "Homme, " + date.toLocaleDateString('et-EE', optionsShort);
    } else {
      return date.toLocaleDateString('et-EE', options);
    }
  }

}
