import { ScheduleModel } from './schedule.model';

export class ShowTimeModel {
  public city: number;
  public schedule: ScheduleModel;
  public scheduleArray: string[];
  public cinema: number;
  public date: string;
}
