import { ShowTimeModel } from './showtime.model';

export class MovieModel {
  public id: string;
  public title: string;
  public title_eng: string;
  public poster: string;
  public restriction: string;
  public rating: any;
  public genres_est: string;
  public year: number;
  public duration: number;
  public durationStr: string;
  public distributor: string;
  public description: string;
  public release_date: string;
  public videos: string[];
  public gallery: string[];
  public showtimes: ShowTimeModel[];
  public has3D: boolean;
}
